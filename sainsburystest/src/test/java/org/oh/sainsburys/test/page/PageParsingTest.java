package org.oh.sainsburys.test.page;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

public class PageParsingTest {
	private static final BigDecimal TEST_PRICE_3_50 = new BigDecimal("3.50"); 
	
	//TODO use some object conatiner to inject these instances
	private PageParser subject = new PageParserImpl();
	
	private String mainPage;
	private String productPage;
	private String productPageWrongPrice;
	private String productPageMissingData;
	
	@Before
	public void init() {
		mainPage = readDataFromFile("main_page.html");
		productPage = readDataFromFile("product_page1.html");
		productPageWrongPrice = readDataFromFile("product_page_wrong_price.html");
		productPageMissingData = readDataFromFile("product_page_no_entry.html");
	}
	
	@Test
	public void whenPageIsEmptyReturnEmptyList() {
		List<String> result = subject.parseMainPage("");
		
		assertNotNull("Result must be not null", result);
		assertTrue("List must be empty", result.size()==0);
	}
	
	@Test
	public void whenPageContainsProductsUrlsReturnThem() {
		List<String> result = subject.parseMainPage(mainPage);
		
		assertNotNull("Result must be not null", result);
		assertTrue("List must be empty", result.size()==7);
		for (String url : result) {
			assertTrue("Wrong url", url.startsWith("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/"));
		}
	}
	
	@Test
	public void whenProductPageIsEmptyReturnNull() {
		LinkedPageDescription data = subject.parseLinkedPage("");
		assertNull(data);
	}
	
	@Test
	public void whenProductPageContainsDataCorrectDescriptionIsRetrieved() {
		LinkedPageDescription data = subject.parseLinkedPage(productPage);
		assertNotNull(data);
		assertEquals(39185, data.getPageSize());
		assertEquals(TEST_PRICE_3_50, data.getProductDescription().getUnitPrice());
		assertEquals("Apricots", data.getProductDescription().getDescription());
		assertEquals("Sainsbury's Apricot Ripe & Ready x5", data.getProductDescription().getTitle());
	}
	
	@Test
	public void whenProductPriceFormatIsWrongNullIsReturned() {
		LinkedPageDescription data = subject.parseLinkedPage(productPageWrongPrice);
		assertNull(data);
	}
	
	@Test
	public void whenProductPriceHasMissingDataNullIsReturned() {
		LinkedPageDescription data = subject.parseLinkedPage(productPageMissingData);
		assertNull(data);
	}
	
	private String readDataFromFile(String name) {
		try (Scanner scanner = new Scanner(PageParsingTest.class.getResourceAsStream("/"+name), "UTF-8");){
			return scanner.useDelimiter("\\A").next();
		}
	}
}
