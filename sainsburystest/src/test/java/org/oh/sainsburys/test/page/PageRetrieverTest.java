package org.oh.sainsburys.test.page;

import static org.junit.Assert.assertNull;

import org.junit.Ignore;
import org.junit.Test;

public class PageRetrieverTest {

	//TODO use some object conatiner to inject these instances
	private PageRetriever subject = new PageRetrieverImpl();
	
	@Ignore
	@Test
	public void whenURLisCorrectDataFromPageIsLoaded() {
		// to test retriever we would neet to start HTTP server as part of test
		// and attempt to retrieve test conetnt from localhot
		// this is out of scope of this exercise
	}
	
	@Test
	public void whenURLisNotReachableNullIsReturned() {
		String content = subject.retrievePage("http://localhost/no/such/path");
		assertNull(content);
	}
}
