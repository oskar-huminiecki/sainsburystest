package org.oh.sainsburys.test.presentation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;
import org.oh.sainsburys.test.page.LinkedPageDescription;
import org.oh.sainsburys.test.page.ProductData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PresentationTest {
	private static final Logger logger = LoggerFactory.getLogger(PresentationTest.class);
	private ObjectMapper mapper = new ObjectMapper();
	
	//TODO use some object conatiner to inject these instances
	private Serialiser subject = new SerialiserImpl();
	
	@Test
	public void whenNoProductItemSumIsZero() {
		List<LinkedPageDescription> productPagesDescriptions = Collections.emptyList();  
		String result = subject.serialiseProductSummary(productPagesDescriptions);
		JsonNode root = parseJSON(result);

		JsonNode results = root.get("results");
		assertNotNull(results);
		assertEquals("results must be empty", 0, results.size());
		
		JsonNode total = root.get("total");
		assertNotNull(total);
		assertEquals("total must be 0.00", "0.00", total.asText());
	}
	
	@Test
	public void whenProductItemsProvidedAllDetailsArePresentAndSumIsCorrect()  {
		List<LinkedPageDescription> productPagesDescriptions = new ArrayList<>();
		productPagesDescriptions.add(new LinkedPageDescription(92774, new ProductData("Title1", "Description1", new BigDecimal("1.80"))));
		productPagesDescriptions.add(new LinkedPageDescription(89088, new ProductData("Title2", "Description2", new BigDecimal("2.00"))));
		
		String result = subject.serialiseProductSummary(productPagesDescriptions);
		JsonNode root = parseJSON(result);
		JsonNode total = root.get("total");
		assertNotNull(total);
		assertEquals("total must be 3.80", "3.80", total.asText());
		
		JsonNode results = root.get("results");
		assertNotNull(results);
		assertEquals("results must have two entries", 2, results.size());
		
		results.forEach(new Consumer<JsonNode>() {
			@Override
			public void accept(JsonNode child) {
				JsonNode title = child.get("title");
				assertNotNull(title);
				JsonNode size = child.get("size");
				assertNotNull(size);
				JsonNode price = child.get("unit_price");
				assertNotNull(price);
				JsonNode description = child.get("description");
				assertNotNull(description);
				//
				switch (title.asText()) {
					case "Title1":
						assertEquals("Description1", description.asText());
						assertEquals("90.6kb", size.asText());
						assertEquals("1.80", price.asText());
						break;
					case "Title2":
						assertEquals("Description2", description.asText());
						assertEquals("87kb", size.asText());
						assertEquals("2.00", price.asText());
						break;
					default:
						fail("title has unexpected value: " + title.asText());
				}
			}
		});
	}
	
	private JsonNode parseJSON(String json) {
		try (Reader reader = new StringReader(json)) {
			return mapper.readValue(reader, JsonNode.class);			
		} catch (Exception e) {
			logger.error("Exception while parsing json", e);
			throw new RuntimeException(e);
		}		  
	}
}
