package org.oh.sainsburys.test.page;

public interface PageRetriever {

	/**
	 * Return content of page under url or null
	 * @param url
	 * @return retrieved page or null
	 */
	String retrievePage(String url);

}
