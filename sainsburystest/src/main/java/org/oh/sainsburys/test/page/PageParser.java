package org.oh.sainsburys.test.page;

import java.util.List;

public interface PageParser {
	
	/**
	 * Retrieve data from product page
	 * @param data
	 * @return extracted data from page or null when no data or incomplete data present on page
	 */
	LinkedPageDescription parseLinkedPage(String data);

	/**
	 * Retrieve list of products' urls from page.
	 * @param data
	 * @return list of products' urls from page or empty list when nothing found 
	 */
	List<String> parseMainPage(String data);
	
}
