package org.oh.sainsburys.test.service;

import java.util.ArrayList;
import java.util.List;

import org.oh.sainsburys.test.page.LinkedPageDescription;
import org.oh.sainsburys.test.page.PageParser;
import org.oh.sainsburys.test.page.PageParserImpl;
import org.oh.sainsburys.test.page.PageRetriever;
import org.oh.sainsburys.test.page.PageRetrieverImpl;
import org.oh.sainsburys.test.presentation.Serialiser;
import org.oh.sainsburys.test.presentation.SerialiserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductDataServiceImpl implements ProductDataService{
	private static final Logger logger = LoggerFactory.getLogger(ProductDataServiceImpl.class);
	
	//TODO use some object conatiner to inject these instances 
	private PageRetriever pageRetriever = new PageRetrieverImpl();
	private PageParser pageParser = new PageParserImpl();
	private Serialiser serialiser = new SerialiserImpl();
	
	@Override	
	public String retrieveAndSerialiseProductData(String url) {
		String mainPage = pageRetriever.retrievePage(url);
		if (mainPage == null) {
			logger.error("Can't retrieve data from url=" + url);
			return null;
		}
		
		List<String> productLinks = pageParser.parseMainPage(mainPage);
		
		List<LinkedPageDescription> linkedPageDescriptions = new ArrayList<>(productLinks.size());
		for (String linkedPageUrl : productLinks) {
			LinkedPageDescription linkedPageDescription = processProductPage(linkedPageUrl);
			if (linkedPageDescription != null) {
				linkedPageDescriptions.add(linkedPageDescription);
			}
		}
		
		String serialisedData = serialiser.serialiseProductSummary(linkedPageDescriptions);
		return serialisedData;
	}

	private LinkedPageDescription processProductPage(String linkedPageUrl) {
		LinkedPageDescription linkedPageDescription = null;
		String productPage = pageRetriever.retrievePage(linkedPageUrl);
		if (productPage == null) {
			logger.error("Can't retrieve data from url=" + linkedPageUrl);			
		} else {
			linkedPageDescription = pageParser.parseLinkedPage(productPage);
			if (linkedPageDescription == null) {
				logger.error("No product data in url=" + linkedPageUrl);
			}
		}
		return linkedPageDescription;
	}

}
