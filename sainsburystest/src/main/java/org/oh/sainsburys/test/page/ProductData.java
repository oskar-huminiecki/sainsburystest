package org.oh.sainsburys.test.page;

import java.math.BigDecimal;

public class ProductData {
	private String title;
	private String description;
	private BigDecimal unitPrice;
	
	public ProductData(String title, String description, BigDecimal unitPrice) {
		if (title == null) {
			throw new IllegalArgumentException("Title must not be null");
		}
		if (description == null) {
			throw new IllegalArgumentException("Descriptionmmust not be null");
		}
		if (unitPrice == null) {
			throw new IllegalArgumentException("Unit price must not be null");
		}
		//TODO - verify with PO if unit price can be negative
		
		this.title = title;
		this.description = description;
		this.unitPrice = unitPrice;
	}
	
	public String getTitle() {
		return title;
	}
	public String getDescription() {
		return description;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	@Override
	public String toString() {
		return "ProductData [title=" + title + ", description=" + description
				+ ", unitPrice=" + unitPrice + "]";
	}
}
