package org.oh.sainsburys.test.presentation;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.oh.sainsburys.test.page.LinkedPageDescription;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SerialiserImpl implements Serialiser {
	private final static Logger logger = LoggerFactory.getLogger(SerialiserImpl.class);
	private ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public String serialiseProductSummary(List<LinkedPageDescription> items) {
		String serialisedData = null;
		try (Writer writer = new StringWriter();){
			ResultDto resultDto = ResultDto.createFromPageDescriptions(items);
			mapper.writeValue(writer, resultDto);
			serialisedData = writer.toString();
		} catch (IOException e) {
			logger.error("Can't serialise DTO", e);
		}
		return serialisedData;
	}	
	
}
