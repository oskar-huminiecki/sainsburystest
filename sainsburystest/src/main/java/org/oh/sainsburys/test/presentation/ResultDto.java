package org.oh.sainsburys.test.presentation;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.oh.sainsburys.test.page.LinkedPageDescription;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * Required presentation is different than chosen data model so I use this DTO as presentation object 
 * If different presentation would be required in future just new DTO could be defined and underlying data object could stay unchanged
 */
final class ResultDto {
	private static final NumberFormat formatter;
	static {
		formatter = NumberFormat.getInstance();
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(2);
	}
	
	private List<ResultItemDto> items;
	private String total;
	
	private ResultDto(){}
	
	public static ResultDto createNew (List<ResultItemDto> items, BigDecimal total) {
		ResultDto result = new ResultDto();
		result.items = Collections.unmodifiableList(items); //make DTO immutable
		result.total = formatTotal(total);
		return result;
	}
	
	public static ResultDto createFromPageDescriptions (List<LinkedPageDescription> descriptions) {
		List<ResultItemDto> tmp = new ArrayList<>();
		BigDecimal sum = BigDecimal.ZERO;
		for (LinkedPageDescription linkedPageDescription : descriptions) {
			ResultItemDto dto = ResultItemDto.createFromLinkedPageDescription(linkedPageDescription);
			tmp.add(dto);
			sum = sum.add(linkedPageDescription.getProductDescription().getUnitPrice());
		}
		return createNew(tmp, sum);
	}
	
	
	protected final static String formatTotal(BigDecimal total) {
		return formatter.format(total);
	}

	@JsonProperty("results")
	public List<ResultItemDto> getItems() {
		return items;
	}

	public String getTotal() {
		return total;
	}
}
