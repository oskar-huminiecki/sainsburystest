package org.oh.sainsburys.test.presentation;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.oh.sainsburys.test.page.LinkedPageDescription;

import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * Required presentation is different than chosen data model so I use this DTO as presentation object 
 * If different presentation would be required in future just new DTO could be defined and underlying data object could stay unchanged
 */
final class ResultItemDto {
	private static final NumberFormat priceFormatter;
	static {
		priceFormatter = NumberFormat.getInstance();
		priceFormatter.setMaximumFractionDigits(2);
		priceFormatter.setMinimumFractionDigits(2);
	}
	private static final NumberFormat sizeFormatter;
	static {
		sizeFormatter = NumberFormat.getInstance();
		sizeFormatter.setMaximumFractionDigits(1);
		sizeFormatter.setMinimumFractionDigits(0);
	}
	
	private String title;
	private String description;
	private String unitPrice;
	private String size;
	
	private ResultItemDto(){}
	
	/*
	 * Local static factory
	 */
	public static ResultItemDto createFromLinkedPageDescription(LinkedPageDescription linkedPageDescription) {
		ResultItemDto resultItem = new ResultItemDto();
		resultItem.title = linkedPageDescription.getProductDescription().getTitle(); 
		resultItem.description = linkedPageDescription.getProductDescription().getDescription();
		resultItem.unitPrice = formatPrice(linkedPageDescription.getProductDescription().getUnitPrice());
		resultItem.size = formatSizeInkb(linkedPageDescription.getPageSize());
		return resultItem;
	}

	protected final static String formatPrice(BigDecimal price) {
		return priceFormatter.format(price);
	}
	
	protected final static String formatSizeInkb(int size) {
		return sizeFormatter.format((float)size/1024.0f) + "kb";
	}
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	@JsonProperty("unit_price")
	public String getUnitPrice() {
		return unitPrice;
	}

	public String getSize() {
		return size;
	}
}
