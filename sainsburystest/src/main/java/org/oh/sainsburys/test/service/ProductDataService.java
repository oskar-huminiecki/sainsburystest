package org.oh.sainsburys.test.service;

public interface ProductDataService {
	
	/**
	 * Scan products pages and return JSON result 
	 * @param url
	 * @return
	 */
	String retrieveAndSerialiseProductData(String url);
	
}
