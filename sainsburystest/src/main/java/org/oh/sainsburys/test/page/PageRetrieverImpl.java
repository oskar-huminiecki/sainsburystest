package org.oh.sainsburys.test.page;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageRetrieverImpl implements PageRetriever {
	private static Logger logger = LoggerFactory.getLogger(PageRetrieverImpl.class);
	
	@Override
	public String retrievePage(String url) {
		String data = null;
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		try {
			response = client.execute(new HttpGet(url));
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				data = EntityUtils.toString(response.getEntity());
			}
		} catch (IOException e) {
			logger.error("Can't get data from url=" + url, e);
		}
		return data;
	}

}
