package org.oh.sainsburys.test.page;

public class LinkedPageDescription {
	/*
	 * Page size and product description are two separate concerns so I keep product details in separate structure
	 */
	private int pageSize;
	private ProductData productDescription;

	public LinkedPageDescription(int pageSize, ProductData productDescription) {
		this.pageSize = pageSize;
		this.productDescription = productDescription;
	}

	public int getPageSize() {
		return pageSize;
	}
	public ProductData getProductDescription() {
		return productDescription;
	}

	@Override
	public String toString() {
		return "LinkedPageDescription [pageSize=" + pageSize
				+ ", productDescription=" + productDescription + "]";
	}
}
