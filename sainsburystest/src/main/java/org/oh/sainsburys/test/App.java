package org.oh.sainsburys.test;

import java.net.MalformedURLException;

import org.oh.sainsburys.test.service.ProductDataService;
import org.oh.sainsburys.test.service.ProductDataServiceImpl;

public class App {
	
	private static ProductDataService service = new ProductDataServiceImpl();
	private static final String DEFAULT_URL="http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html";
	
	public static void main(String[] args) throws MalformedURLException {
		String url = DEFAULT_URL;
		if (args.length>0) {
			url = args[0];
		}
		String result = service.retrieveAndSerialiseProductData(url);
		
		System.out.println(result==null?"Failed to get any data!":result);
	}
	
}
