package org.oh.sainsburys.test.page;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageParserImpl implements PageParser {
	private static final Logger logger = LoggerFactory.getLogger(PageParserImpl.class);
	
	@Override
	public LinkedPageDescription parseLinkedPage(String data) {
		LinkedPageDescription result = null;
		Document doc = Jsoup.parse(data);
		
		Element descriptionElement = doc.select("div.productText > p").first();
		String description = descriptionElement==null?null:descriptionElement.text();
		
		Element titleElement = doc.select("div.productTitleDescriptionContainer > h1").first();
		String title = titleElement==null?null:titleElement.text();
		
		Element priceElement = doc.select("p.pricePerUnit").first();
		BigDecimal price = null;
		String priceTxt = priceElement==null?null:priceElement.text();
		if (priceTxt != null) {
			priceTxt = priceTxt.replaceAll("[^0-9?!\\.]",""); //remove all but digits and decimal point
			try {
				price = new BigDecimal(priceTxt);
			} catch (NumberFormatException e) {
				logger.error("Wrong price format" + priceTxt);
			}
		}

		if (description != null && title != null && price != null) {
			result = new LinkedPageDescription(data.length(), new ProductData(title, description, price));
		}
		return result;
	}

	@Override
	public List<String> parseMainPage(String data) {
		List<String> urls = new ArrayList<>();
		Document doc = Jsoup.parse(data);
		Elements productInfoElements = doc.select(".productInfo");
		for (Element element : productInfoElements) {
			Element link = element.select("h3 > a").first();
			if (link != null) {
				String url = link.attr("href");
				urls.add(url);
			}
		}
		return urls;
	}

}
