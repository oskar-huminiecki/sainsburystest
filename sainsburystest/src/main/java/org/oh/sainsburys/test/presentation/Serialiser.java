package org.oh.sainsburys.test.presentation;

import java.util.List;

import org.oh.sainsburys.test.page.LinkedPageDescription;

public interface Serialiser {
	
	/**
	 * Returns JSON representation of product lists. In addition calculates sum of item prices 
	 * @param items
	 * @return JSON
	 */
	String serialiseProductSummary(List<LinkedPageDescription> items);
	
}
