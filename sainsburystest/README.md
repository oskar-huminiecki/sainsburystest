to test: mvn clean compile test
to run: mvn -q clean compile exec:java
to run with alternative url: mvn -q exec:java -Dexec.args="url_to_use" 

Requires: Java8, Maven (tested on Maven 3.3.3)